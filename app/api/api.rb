class API < Grape::API
  prefix 'api'
  format :json
  mount PageContent::V1
end