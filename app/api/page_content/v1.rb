module PageContent
	class V1 < Grape::API
		require 'nokogiri'
		require 'open-uri'
		resource :v1 do
			post :storeContent do
				if params[:url].present?
					begin
						content_url = params[:url]
						if content_url.include?('http')
							content_url = content_url
						else
							content_url = "http://#{content_url}"
						end
						url = Url.find_by_name(content_url)

						if !url.present?
							new_url = Url.create(name: content_url)
						  	doc = Nokogiri::HTML(open(content_url))
						  	doc.css('h1').each do |d|
						  		new_url.h1s.create(name: d.content)
						  	end
						  	doc.css('h2').each do |d|
						  		new_url.h2s.create(name: d.content)
						  	end
						  	doc.css('h3').each do |d|
						  		new_url.h3s.create(name: d.content)
						  	end
						  	doc.css('a').each do |d|
						  		new_url.links.create(name: d['href'])
						  	end
						end
						"Page content for url: #{params[:url]} has been stored."
					rescue Exception => e
					  	"Couldn't read \"#{ url }\": #{ e }"
					end
				else
					'Something is missing. Please make sure there must be url parameter in your request.'
				end
			end

			get :listContent do
				return_message = {}
				i = 1
				urls = Url.all
				urls.each do |u|
					u_stats = {}
					u_stats["url"] = u.name
					u_stats["H1"] = u.h1s.pluck(:name)
					u_stats["H2"] = u.h2s.pluck(:name)
					u_stats["H3"] = u.h3s.pluck(:name)
					u_stats["Links"] = u.linkss.pluck(:name)
					return_message[i] = u_stats
					i += 1
				end
				return_message
			end
		end
	end
end